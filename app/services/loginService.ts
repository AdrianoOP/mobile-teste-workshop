const URL_BASE = "https://qav3.datalife.global";
const END_POINT_LOGIN = "/loginweb";

let token = "";

export async function login(username: string, password: string) {
    const urlFetch = URL_BASE + END_POINT_LOGIN;
    const bodyInit = {
        username,
        password
    }
    const ret = await fetch(urlFetch, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        body: JSON.stringify(bodyInit)
    });
    
    if (!ret.ok) {
        throw Error("Não foi possível acessar a API")
    }
    const resposta = await ret.json();
    
    if (resposta.token && resposta.token !== "") {
        token = resposta.token;
        return resposta.details; // informação do meu usuário
    }
    throw Error("Por algum motivo não conseguiu logar")
}

export function getAuthToken() {
    return token;
}