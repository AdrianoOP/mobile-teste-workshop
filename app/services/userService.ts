import * as loginService from "../services/loginService";

const URL_BASE = "https://qav3.datalife.global";
const END_POINT_USERS = "/user";

export type User = {
    id: number;
    name: string;
    username: string;
    email: string;
}

export async function getListUsersWithNameUsername(): Promise<Array<User>> {
    const urlFetch = URL_BASE + END_POINT_USERS + "/listNameUsername";
    const token = loginService.getAuthToken();

    const ret = await fetch(urlFetch, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": `Bearer ${token}`
        },
    });
    
    if (!ret.ok) {
        throw Error("Não foi possível acessar a API")
    }
    return ret.json();
}